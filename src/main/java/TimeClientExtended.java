import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Base64;

/**
 * @author Kacper Urbaniec
 * @version 2020-05-04
 */
public class TimeClientExtended {
    public static void main(String[] args)  {
        try {
            // Establish connection
            Socket s = new Socket("time.nist.gov", 13);
            // Get stream
            BufferedReader reader = new BufferedReader(
                new InputStreamReader((s.getInputStream()))
            );
            // Read via bytes https://stackoverflow.com/a/5690997/12347616
            byte[] resultBuff = new byte[0];
            byte[] buff = new byte[32];
            int k = -1;
            while((k = s.getInputStream().read(buff, 0, buff.length)) > -1) {
                byte[] tbuff = new byte[resultBuff.length + k];
                System.arraycopy(resultBuff, 0, tbuff, 0, resultBuff.length);
                System.arraycopy(buff, 0, tbuff, resultBuff.length, k);
                resultBuff = tbuff;
            }
            String text = new String(resultBuff);
            System.out.println(text);
            // Close all resources
            reader.close();
            s.close();
        } catch (Exception ex) {
            // Print encountered errors
            System.out.println("Exception encountered:");
            ex.printStackTrace();
        }
    }
}
