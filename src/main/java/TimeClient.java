import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * @author Kacper Urbaniec
 * @version 2020-05-04
 */
public class TimeClient {
    public static void main(String[] args)  {
        try {
            // Establish connection
            Socket s = new Socket("time.nist.gov", 13);
            // Get stream
            BufferedReader reader = new BufferedReader(
                new InputStreamReader((s.getInputStream()))
            );
            // Read text via lines
            String text;
            while ((text = reader.readLine()) != null) {
                System.out.println(text);
            }
            // Close all resources
            reader.close();
            s.close();
        } catch (Exception ex) {
            // Print encountered errors
            System.out.println("Exception encountered:");
            ex.printStackTrace();
        }
    }
}
